package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/go-training/final/model"
	"gitlab.com/go-training/final/model/request"
)

type CustomerRepoImpl struct {
	mock.Mock
}

func (self *CustomerRepoImpl) Create(note *model.Customer) (*model.Customer, error) {
	args := self.Called(note)
	out := args.Get(0)
	if out == nil {
		return nil, args.Error(1)
	}
	return out.(*model.Customer), args.Error(1)
}

func (self *CustomerRepoImpl) Find(id uint) (*model.Customer, error) {
	args := self.Called(id)
	out := args.Get(0)
	if out == nil {
		return nil, args.Error(1)
	}
	return out.(*model.Customer), args.Error(1)
}

func (self *CustomerRepoImpl) Delete(id uint) (bool, error) {
	args := self.Called(id)
	return args.Get(0).(bool), args.Error(0)
}

func (self *CustomerRepoImpl) Update(id uint, info request.CustomerUpdateInfo) (*model.Customer, error) {
	args := self.Called(id, info)
	out := args.Get(0)
	if out == nil {
		return nil, args.Error(1)
	}
	return out.(*model.Customer), args.Error(1)
}
