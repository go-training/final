package handler

import (
	"errors"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/iris-contrib/httpexpect"
	"github.com/kataras/iris"
	"github.com/kataras/iris/httptest"
	"github.com/stretchr/testify/suite"
	"gitlab.com/go-training/final/mock"
	"gitlab.com/go-training/final/model"
)

type CustomerHandlerTestSuite struct {
	suite.Suite
	customerRepo *mock.CustomerRepoImpl
	Expect       *httpexpect.Expect
}

func TestCustomerHandlerTestSuite(t *testing.T) {
	suite.Run(t, new(CustomerHandlerTestSuite))
}

func (s *CustomerHandlerTestSuite) SetupTest() {
	logger := log.New(os.Stdout, "", 0)

	app := iris.Default()
	s.customerRepo = new(mock.CustomerRepoImpl)
	customerHanler := &customerHandlerImpl{
		customerRepo: s.customerRepo,
		log:          logger,
	}
	customerHanler.inject(app)
	s.Expect = httptest.New(s.T(), app)
}

func (s *CustomerHandlerTestSuite) TestCustomer() {
	s.Run("Test find a note has found", func() {
		var customerID uint = 49
		out := &model.Customer{}
		out.ID = customerID
		s.customerRepo.On("Find", customerID).Return(out, nil)
		url := fmt.Sprintf("/customer/%d", customerID)
		expect := s.Expect.GET(url).Expect()
		expect.Status(httptest.StatusOK)
		expect.JSON().Object().ContainsKey("ID").ValueEqual("ID", customerID)
	})
	s.Run("Test find a note not found", func() {
		var customerID uint = 50
		out := &model.Customer{}
		s.customerRepo.On("Find", customerID).Return(out, errors.New("Not found"))
		url := fmt.Sprintf("/customer/%d", customerID)
		expect := s.Expect.GET(url).Expect()
		expect.Status(httptest.StatusNotFound)
	})
}
