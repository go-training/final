package handler

import (
	"log"

	"github.com/kataras/iris"
	"gitlab.com/go-training/final/repo"
)

type productHandlerImpl struct {
	productRepo repo.ProductRepo
	log         *log.Logger
}

func (handler productHandlerImpl) inject(app *iris.Application) {
	group := app.Party("api/v1/products")
	group.Get("/{id:uint}", handler.GetInfoById)
}

// @Description Get product info by ID
// @Tags products
// @Accept  json
// @Produce  json
// @Param   id     path    uint     true        "Product ID"
// @Success 200 {object} response.ProductInfo
// @Failure 404 {object} handler.StatusError
// @Router /products/{id} [get]
func (handler productHandlerImpl) GetInfoById(c iris.Context) {
	id := c.Params().GetUintDefault("id", 0)
	productInfo, err := handler.productRepo.GetInfo(id)
	if err != nil {
		simpleReturnHandler(c, nil, NotFound(err))
		return
	}
	simpleReturnHandler(c, productInfo, nil)
}
