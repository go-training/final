package handler

import (
	"log"

	"github.com/iris-contrib/swagger"
	"github.com/iris-contrib/swagger/swaggerFiles"
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"github.com/urfave/cli"
	docs "gitlab.com/go-training/final/docs"
	"gitlab.com/go-training/final/repo"
)

func BuildEngine(appContext *cli.Context, logger *log.Logger, db *gorm.DB) *iris.Application {
	app := iris.Default()
	app.Logger().SetLevel(appContext.GlobalString("loglevel"))

	/* Swagger */

	docs.SwaggerInfo.BasePath = "/api/v1"
	app.Get("/swagger/*any}", swagger.WrapHandler(swaggerFiles.Handler))

	/* End Swagger */

	// HealCheck
	healCheckHandlerImpl := healthCheckHandlerImpl{
		log: logger,
	}
	healCheckHandlerImpl.inject(app)
	// Customer handler
	customerHandlerImpl := customerHandlerImpl{
		customerRepo: repo.CustomerRepoImpl{
			DB: db,
		},
		log: logger,
	}
	customerHandlerImpl.inject(app)

	// Product handler
	productHandlerImpl := productHandlerImpl{
		productRepo: repo.ProductRepoImpl{
			DB: db,
		},
		log: logger,
	}
	productHandlerImpl.inject(app)

	return app
}

func simpleReturnHandler(c iris.Context, result interface{}, err Error) {
	if err != nil {
		c.StatusCode(err.Status())
		c.JSON(iris.Map{
			"error": err.Error(),
		})
		return
	}
	c.JSON(result)
}
