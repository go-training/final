package handler

import (
	"log"

	"github.com/kataras/iris"
	"gitlab.com/go-training/final/model"
	"gitlab.com/go-training/final/model/request"
	"gitlab.com/go-training/final/repo"
)

type customerHandlerImpl struct {
	customerRepo repo.CustomerRepo
	log          *log.Logger
}

func (handler customerHandlerImpl) inject(app *iris.Application) {
	group := app.Party("api/v1/customers")
	group.Get("/{id:uint}", handler.GetById)
	group.Post("", handler.AddNew)
	group.Delete("/{id:uint}", handler.DeleteById)
	group.Put("/{id:uint}", handler.UpdateById)
}

// @Description Get customer info by ID
// @Tags customers
// @Accept  json
// @Produce  json
// @Param   id     path    uint     true        "Some ID"
// @Success 200 {object} model.Customer
// @Failure 404 {object} handler.StatusError
// @Router /customers/{id} [get]
func (handler customerHandlerImpl) GetById(c iris.Context) {
	id := c.Params().GetUintDefault("id", 0)
	customer, err := handler.customerRepo.Find(id)
	if err != nil {
		simpleReturnHandler(c, nil, NotFound(err))
		return
	}
	simpleReturnHandler(c, customer, nil)
}

// @Description Add new customer info
// @Tags customers
// @Accept  json
// @Produce  json
// @Param   user     body    model.Customer     true        "Customer info"
// @Success 200 {object} model.Customer
// @Failure 404 {object} handler.StatusError
// @Router /customers [post]
func (handler customerHandlerImpl) AddNew(c iris.Context) {
	customer := &model.Customer{}
	err := c.ReadJSON(&customer)
	if err != nil {
		simpleReturnHandler(c, nil, BadRequest(err))
		return
	}
	_, err = handler.customerRepo.Create(customer)
	if err != nil {
		simpleReturnHandler(c, nil, BadRequest(err))
		return
	}
	simpleReturnHandler(c, customer, nil)
}

// @Description Delete customer
// @Tags customers
// @Accept  json
// @Produce  json
// @Param   id     path    uint     true        "Customer ID"
// @Success 200 {string} string	"true"
// @Failure 404 {object} handler.StatusError
// @Router /customers/{id} [delete]
func (handler customerHandlerImpl) DeleteById(c iris.Context) {
	id := c.Params().GetUintDefault("id", 0)
	_, err := handler.customerRepo.Find(id)
	if err != nil {
		simpleReturnHandler(c, nil, NotFound(err))
		return
	}
	result, err := handler.customerRepo.Delete(id)
	if err != nil {
		simpleReturnHandler(c, nil, BadRequest(err))
		return
	}
	simpleReturnHandler(c, result, nil)
}

// @Description Update customer
// @Tags customers
// @Accept  json
// @Produce  json
// @Param   id     			path    uint     						true       "Customer ID"
// @Param   updateInfo     	body    request.CustomerUpdateInfo     	true       "Update Info"
// @Success 200 {object} model.Customer
// @Failure 404 {object} handler.StatusError
// @Router /customers/{id} [put]
func (handler customerHandlerImpl) UpdateById(c iris.Context) {
	id := c.Params().GetUintDefault("id", 0)
	updateInfo := request.CustomerUpdateInfo{}
	err := c.ReadJSON(&updateInfo)
	if err != nil {
		simpleReturnHandler(c, nil, BadRequest(err))
		return
	}
	_, err = handler.customerRepo.Find(id)
	if err != nil {
		simpleReturnHandler(c, nil, NotFound(err))
		return
	}
	result, err := handler.customerRepo.Update(id, updateInfo)
	if err != nil {
		simpleReturnHandler(c, nil, BadRequest(err))
		return
	}
	simpleReturnHandler(c, result, nil)
}
