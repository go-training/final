package response

// Prodcut info
type ProductInfo struct {
	ID           uint
	Name         string
	CategoryID   uint
	CategoryName string
}
