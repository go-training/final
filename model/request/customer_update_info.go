package request

// Model Update Customer Info
type CustomerUpdateInfo struct {
	Address string
	Phone   string
}
