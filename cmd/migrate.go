package cmd

import (
	"gitlab.com/go-training/final/model"
	"github.com/urfave/cli"
)

func migrateAction(appContext *cli.Context) {
	db := newDB(appContext)
	defer db.Close()
	db.AutoMigrate(&model.Customer{}, &model.Product{}, &model.Category{})
}

var Migrate = cli.Command{
	Name:  "migrate",
	Usage: "Migrate db",
	Action: func(appContext *cli.Context) error {
		migrateAction(appContext)
		return nil
	},
}
