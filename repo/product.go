package repo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/go-training/final/model"
	"gitlab.com/go-training/final/model/response"
)

type ProductRepo interface {
	Find(uint) (*model.Product, error)
	GetInfo(uint) (response.ProductInfo, error)
}

type ProductRepoImpl struct {
	DB *gorm.DB
}

// Find Product
func (productRepo ProductRepoImpl) Find(id uint) (*model.Product, error) {
	product := &model.Product{}
	error := productRepo.DB.Find(product, id).Error
	return product, error
}

// Get Product Info
func (productRepo ProductRepoImpl) GetInfo(id uint) (response.ProductInfo, error) {
	productInfo := response.ProductInfo{}
	err := productRepo.DB.Raw("select products.id, products.name, categories.id as CategoryID, categories.name as categoryName "+
		" from products left join categories on categories.id = products.category_id "+
		" where products.id = ? ", id).
		Scan(&productInfo).Error
	return productInfo, err
}
