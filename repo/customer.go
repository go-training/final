package repo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/go-training/final/model"
	"gitlab.com/go-training/final/model/request"
)

type CustomerRepo interface {
	Create(*model.Customer) (*model.Customer, error)
	Find(uint) (*model.Customer, error)
	Delete(uint) (bool, error)
	Update(uint, request.CustomerUpdateInfo) (*model.Customer, error)
}

type CustomerRepoImpl struct {
	DB *gorm.DB
}

// Add a customer
func (customerRepo CustomerRepoImpl) Create(customer *model.Customer) (*model.Customer, error) {
	err := customerRepo.DB.Create(&customer).Error
	return customer, err
}

// Find a customer
func (customerRepo CustomerRepoImpl) Find(id uint) (*model.Customer, error) {
	customer := &model.Customer{}
	err := customerRepo.DB.Find(customer, id).Error
	return customer, err
}

// Delete a customer
func (customerRepo CustomerRepoImpl) Delete(id uint) (bool, error) {
	customer := &model.Customer{}
	err := customerRepo.DB.Find(customer, id).Error
	if err != nil {
		return false, err
	}
	err = customerRepo.DB.Delete(customer).Error
	if err != nil {
		return false, err
	}
	return true, nil
}

// Update a customer
func (customerRepo CustomerRepoImpl) Update(id uint, info request.CustomerUpdateInfo) (*model.Customer, error) {
	customer := &model.Customer{}
	err := customerRepo.DB.Find(customer, id).Error
	if err != nil {
		return nil, err
	}
	err = customerRepo.DB.Model(customer).Update(
		model.Customer{
			Address: info.Address,
			Phone:   info.Phone,
		}).Error
	if err != nil {
		return nil, err
	}
	return customer, nil
}
